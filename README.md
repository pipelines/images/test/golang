Golang test container image
===

This repository builds a container image suitable for running Go
tests. Some useful features:

* generate Gitlab-compatible test and coverage report artifacts
* properly aggregate coverage data for a whole project (across all its
  sub-packages)

To use, include *ci.yml* from your project's *.gitlab-ci.yml*. Some
useful variables that can be defined:

* `GO_TEST_PACKAGES` - Debian packages to install before running the
  tests
* `GO_TEST_GENERATE` - Set to a non-empty value to run "go generate
  ./..." before running the tests
* `GO_TEST_ARGS` - Additional arguments for "go test" (tags, etc)

