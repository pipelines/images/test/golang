FROM golang:1.23

WORKDIR /tmp
COPY --chmod=0755 run-go-test /usr/bin/run-go-test
RUN go install github.com/boumenot/gocover-cobertura@latest ; \
    go install github.com/jstemmer/go-junit-report@latest
